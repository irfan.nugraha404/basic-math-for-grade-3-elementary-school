import React from "react";
import assets from "../Helpers/assets";

const Container = ({ title, children }) => {
  return (
    <div
      className="grow bg-no-repeat bg-cover flex justify-center items-center lg:px-20 p-5"
      style={{
        backgroundImage: `url(${assets.bg2})`,
        backgroundPosition: "center",
      }}
    >
      <div className="rounded-3xl border-4 border-black bg-white w-full">
        <div className="rounded-3xl border-4 border-black bg-white w-full">
          <div className="font-bold lg:text-5xl text-3xl border-b-4 border-black text-center py-5">
            <span className="px-5">{title}</span>
          </div>
          <div className="p-5">{children}</div>
        </div>
      </div>
    </div>
  );
};

export default Container;
