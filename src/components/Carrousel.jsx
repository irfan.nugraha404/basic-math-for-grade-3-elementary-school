import React, { useRef, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Back from "./Back";
import { useEffect } from "react";

const Carrousel = ({ children, back, submit, error }) => {
  const [slick, setSlick] = useState(null);
  const [currentTab, setCurrentTab] = useState(0);
  const sliderRef = useRef();

  function handleChangeTab(current) {
    setCurrentTab(current);
  }
  function handleNext() {
    if (!children.length) {
      submit()
      return;
    }
    if (currentTab === children.length - 1) {
      submit();
      return;
    }
    slick?.slickNext();
  }
  function handlePrev() {
    if (currentTab === 0) {
      back();
      return;
    }
    slick?.slickPrev();
  }
  function handleChangeRef(slider) {
    sliderRef.current = slider;
  }

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    ref: handleChangeRef,
    afterChange: handleChangeTab,
    asNavFor: slick,
  };

  useEffect(() => {
    setSlick(sliderRef.current);
  }, []);

  return (
    <>
      <Slider {...settings}>{children}</Slider>
      {!!error ? (
        <div className="text-center text-xl font-bold text-rose-600 mt-5">
          {error}
        </div>
      ) : null}
      <div className="flex justify-between items-center gap-2 mt-8">
        <Back onClick={handlePrev} />
        <Back onClick={handleNext} rotate />
      </div>
    </>
  );
};

export default Carrousel;
