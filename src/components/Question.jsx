import React from "react";
import questions from "../Helpers/questions";

const Question = ({ id, question, answers, onChange }) => {
  return (
    <div>
      <div className="flex gap-2">
        <div className="w-4 shrink-0 font-bold lg:text-xl text-sm">{id}.</div>
        <div className="flex flex-col gap-4">
          <span className="font-bold lg:text-xl text-sm">{question}</span>
          <div className="flex flex-col gap-2">
            {answers.map((answer, index) => (
              <div
                key={index}
                className="flex gap-2 items-center py-2 px-3 bg-indigo-300 text-black w-fit"
              >
                <input
                  key={index}
                  name={id}
                  type="radio"
                  id={answer}
                  value={questions.answers[index]}
                  onChange={onChange}
                />
                <label
                  className="font-bold lg:text-xl text-sm"
                  htmlFor={answer}
                >
                  {questions.answers[index]}. {answer}
                </label>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Question;
