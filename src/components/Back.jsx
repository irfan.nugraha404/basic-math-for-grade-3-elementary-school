import React from "react";
import { FaChevronLeft } from "react-icons/fa";
import { useNavigate } from "react-router-dom";

const Back = ({ onClick, rotate }) => {
  const navigate = useNavigate();

  function back() {
    navigate(-1);
  }

  return (
    <div
      className="rounded-full w-fit bg-indigo-300 p-3 cursor-pointer"
      onClick={onClick ? onClick : back}
    >
      {rotate ? (
        <FaChevronLeft className="h-8 w-8 rotate-180" />
      ) : (
        <FaChevronLeft className="h-8 w-8" />
      )}
    </div>
  );
};

export default Back;
