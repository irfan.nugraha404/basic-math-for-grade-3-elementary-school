import React from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Home from "../pages/Home";
import Profile from "../pages/Profile";
import pages from "../Helpers/pages";
import Materi from "../pages/Materi";
import Score from "../pages/Score";
import NotFound from "../pages/NotFound";
import Level from "../pages/Level";
import Video from "../pages/Video";

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={pages.index} element={<Home />} />
        <Route path={pages.profile} element={<Profile />} />
        <Route path={pages.video} element={<Video />} />
        <Route path={pages.level} element={<Level />} />
        <Route path={pages.materi} element={<Materi />} />
        <Route path={pages.score} element={<Score />} />
        <Route path={pages.notFound} element={<NotFound />} />
        <Route path={"*"} element={<Navigate to={pages.notFound} replace />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
