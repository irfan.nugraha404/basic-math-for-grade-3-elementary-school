import React from "react";
import assets from "../Helpers/assets";
import Back from "../components/Back";
import Container from "../components/Container";

const Profile = () => {
  return (
    <Container title="PROFIL">
      <div className="lg:grid flex flex-col-reverse lg:grid-cols-6 gap-5 justify-center items-center">
        <div className="col-span-4 flex flex-col items-center justify-center lg:items-start lg:justify-start">
          <span className="lg:text-2xl font-bold text-center lg:text-start text-xl uppercase">
            Syarifah Annisa
          </span>
          <span className="lg:text-2xl font-bold text-center lg:text-start text-xl">
            18.53.019650
          </span>
          <span className="lg:text-2xl font-bold text-center lg:text-start text-xl uppercase">
            ilmu komputer
          </span>
          <span className="lg:text-2xl font-bold text-center lg:text-start text-xl uppercase">
            universitas muhammadiyah palangka raya
          </span>
        </div>
        <div className="col-span-2 flex justify-center items-center">
          <img src={assets.user} className="rounded-full w-52 h-52" alt="" />
        </div>
      </div>
      <div className="py-5">
        <Back />
      </div>
    </Container>
  );
};

export default Profile;
