import React from "react";
import Container from "../components/Container";
import constants from "../Helpers/constants";
import Back from "../components/Back";
import { useNavigate, useSearchParams } from "react-router-dom";
import pages from "../Helpers/pages";

const Score = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams()
  const materi = searchParams.get(constants.searchParams.materi)
  const result = sessionStorage.getItem(constants.sessionStorage.score);
  function toLevel() {
    navigate(`${pages.level}?materi=${materi}`);
  }
  return (
    <Container title="SCORE">
      <div className="text-3xl text-center">SKOR: {result / 20} / 5</div>
      <div className="text-3xl text-center">NILAI: {result}</div>
      <Back onClick={toLevel} />
    </Container>
  );
};

export default Score;
