import React from "react";
import { Link } from "react-router-dom";
import pages from "../Helpers/pages";

const NotFound = () => {
  return (
    <div className="flex flex-col gap-5 h-screen items-center justify-center font-bold text-base lg:text-5xl">
      Ups, Halaman yang anda cari tidak tersedia
      <Link className="text-rose-600 underline" to={pages.index}>kembali ke home</Link>
    </div>
  );
};

export default NotFound;
