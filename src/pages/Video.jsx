import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Container from "../components/Container";
import Carrousel from "../components/Carrousel";
import constants from "../Helpers/constants";
import pages from "../Helpers/pages";
import assets from "../Helpers/assets";

const Video = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const materi = searchParams.get(constants.searchParams.materi);

  function handleBack() {
    navigate(pages.index);
  }

  function handleSubmit() {
    navigate(`${pages.level}?${constants.searchParams.materi}=${materi}`);
  }

  return (
    <Container title={"Video Pembelajaran " + materi}>
      <Carrousel back={handleBack} submit={handleSubmit}>
        <div className="w-full aspect-video max-h-96">
          <iframe
            src={assets.video[materi]}
            className="w-full h-full"
            frameborder="0"
          />
        </div>
      </Carrousel>
    </Container>
  );
};

export default Video;
