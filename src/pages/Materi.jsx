import React, { useState, useLayoutEffect, useEffect } from "react";
import Container from "../components/Container";
import Carrousel from "../components/Carrousel";
import questions from "../Helpers/questions";
import Question from "../components/Question";
import { useNavigate, useSearchParams, redirect } from "react-router-dom";
import pages from "../Helpers/pages";
import constants from "../Helpers/constants";

const Materi = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const materi = searchParams.get(constants.searchParams.name);
  const level = searchParams.get(constants.searchParams.level);
  const allLevel = JSON.parse(localStorage.getItem(constants.localStorage.level));
  const [answers, setAnswers] = useState({
    number1: "",
    number2: "",
    number3: "",
    number4: "",
    number5: "",
  });
  const [errorMessage, setErrorMessage] = useState("");
  function handleChangeAnswer({ target: { name, value } }) {
    setAnswers((prev) => {
      return {
        ...prev,
        [`number${name}`]: value,
      };
    });

    setErrorMessage("");
  }
  function validate() {
    let valid = true;
    Object.values(answers).map((item) => {
      if (!item) {
        valid = false;
      }
    });
    return valid;
  }
  function handleSubmit() {
    let result = 0;
    const valid = validate();

    if (!valid) {
      setErrorMessage("Isi semua jawaban terlebih dahulu!");
      return;
    }

    const arrAnswers = Object.values(answers);
    arrAnswers.map((item, index) => {
      if (item === questions[materi][level][index].correct) {
        result += 20;
      }
    });

    if (result === 100) {
      allLevel[materi] = Number(level) + 1;
      localStorage.setItem("level", JSON.stringify(allLevel));
    }

    sessionStorage.setItem(constants.sessionStorage.score, result);
    navigate(`${pages.score}?materi=${materi}`);
  }
  function handleBack() {
    navigate(`${pages.level}?materi=${materi}`);
  }

  useEffect(() => {
    const soal = Object.values(constants.materi);
    if (!soal.includes(materi)) {
      navigate(pages.notFound);
    }
  }, []);

  return (
    <Container title={materi.toUpperCase()}>
      <Carrousel back={handleBack} submit={handleSubmit} error={errorMessage}>
        {questions[materi][level]?.map((item) => (
          <Question
            key={item.id}
            id={item.id}
            question={item.question}
            answers={item.answers}
            onChange={handleChangeAnswer}
          />
        ))}
      </Carrousel>
    </Container>
  );
};

export default Materi;
