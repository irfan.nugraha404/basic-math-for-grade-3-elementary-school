import React, { useEffect } from "react";
import menu from "../Helpers/menu";
import { useNavigate } from "react-router-dom";
import assets from "../Helpers/assets";
import pages from "../Helpers/pages";
import constants from "../Helpers/constants";

const Home = () => {
  const navigate = useNavigate();
  function redirect(materi) {
    if (materi) {
      navigate(`${pages.video}?${constants.searchParams.materi}=${materi}`);
      return;
    }

    navigate(pages.profile);
  }

  function storeLevel() {
    if (!localStorage.getItem(constants.localStorage.level)) {
      const level = {
        [constants.materi.penjumlahan]: 1,
        [constants.materi.pengurangan]: 1,
        [constants.materi.perkalian]: 1,
        [constants.materi.pembagian]: 1,
      };

      localStorage.setItem(constants.localStorage.level, JSON.stringify(level));
    }
  }

  useEffect(() => {
    storeLevel();
  }, []);

  return (
    <div
      className="lg:grid lg:grid-cols-2 flex flex-col w-full grow justify-center lg:gap-10 lg:justify-center lg:items-center bg-cover bg-no-repeat lg:px-20 p-10"
      style={{
        backgroundImage: `url(${assets.bg1})`,
        backgroundPosition: "center",
      }}
    >
      <div className="lg:text-5xl mt-5 lg:mt-0 text-xl font-balsamic text-center font-bold mb-10 lg:mb-0">
        Multimedia Interaktif Pembelajaran Matematika Kelas III SD / MI
      </div>
      <div className="flex flex-col gap-5 items-center w-full">
        <span className="lg:text-2xl font-bold">MENU</span>
        {menu.map((item) => (
          <span
            key={item.path}
            onClick={() => redirect(item.name)}
            // onClick={() => navigate(item.path)}
            className="flex justify-center items-center lg:text-2xl rounded-full w-full p-3 border-2 border-black bg-white text-black cursor-pointer hover:bg-gray-300"
          >
            {item.label}
          </span>
        ))}
      </div>
    </div>
  );
};

export default Home;
