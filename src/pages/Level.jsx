import React, { useEffect } from "react";
import assets from "../Helpers/assets";
import { useNavigate, useSearchParams } from "react-router-dom";
import pages from "../Helpers/pages";
import constants from "../Helpers/constants";
import Back from "../components/Back";

const level = [1, 2, 3];

const Level = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const materi = searchParams.get(constants.searchParams.materi);
  const allLevel = JSON.parse(localStorage.getItem(constants.localStorage.level));
  const currentLevel = allLevel[materi];
  const navigate = useNavigate();

  function handleClickLevel(level) {
    navigate(`${pages.materi}?name=${materi}&level=${level}`);
  }

  function handleBack() {
    navigate(`${pages.video}?${constants.searchParams.materi}=${materi}`);
  }

  useEffect(() => {
    const soal = Object.values(constants.materi);
    if (!soal.includes(materi)) {
      navigate(pages.notFound);
    }
  }, []);

  return (
    <div
      className="min-h-screen w-full flex flex-col gap-4 justify-center items-center bg-cover bg-no-repeat"
      style={{
        backgroundImage: `url(${assets.bg1})`,
        backgroundPosition: "center",
      }}
    >
      <div className="text-5xl mb-4">
        {materi}
      </div>
      <div className="w-72 flex flex-col gap-3 items-center justify-center">
        {level.map((item) => (
          <button
            key={item}
            className="flex justify-center items-center lg:text-2xl rounded-full w-full p-3 border-2 border-black bg-white text-black cursor-pointer hover:bg-gray-300 disabled:bg-gray-400 disabled:cursor-default"
            disabled={currentLevel < item}
            onClick={() => handleClickLevel(item)}
          >
            Level {item}
          </button>
        ))}
      </div>
      <div className="w-72">
        <div className="rounded-full border-2 border-black w-fit">
          <Back onClick={handleBack} />
        </div>
      </div>
    </div>
  );
};

export default Level;
