import React from "react";

const Layout = ({ children }) => {
  return (
    <>
      <div className="flex flex-col min-h-screen w-full bg-indigo-300">{children}</div>
    </>
  );
};

export default Layout;
