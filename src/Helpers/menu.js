import constants from "./constants";
import pages from "./pages";

export default [
  {
    label: "Penjumlahan",
    path: pages.penjumlahan,
    name: constants.materi.penjumlahan
  },

  {
    label: "Pengurangan",
    path: pages.pengurangan,
    name: constants.materi.pengurangan
  },

  {
    label: "Perkalian",
    path: pages.perkalian,
    name: constants.materi.perkalian
  },

  {
    label: "Pembagian",
    path: pages.pembagian,
    name: constants.materi.pembagian
  },

  {
    label: "Profil",
    path: pages.profile,
  },
];
