import constants from "./constants";

export default {
  index: "/",
  profile: "/profil",
  penjumlahan: `/materi?name=${constants.materi.penjumlahan}`,
  pengurangan: `/materi?name=${constants.materi.pengurangan}`,
  perkalian: `/materi?name=${constants.materi.perkalian}`,
  pembagian: `/materi?name=${constants.materi.pembagian}`,
  materi: "/materi",
  score: "/score",
  notFound: "/not-found",
  level: "/level",
  video: "/video",
};
