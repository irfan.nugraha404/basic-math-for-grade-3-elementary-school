export default {
  sessionStorage: {
    score: "score",
  },
  localStorage: {
    level: "level"
  },
  materi: {
    penjumlahan: "Penjumlahan",
    pengurangan: "Pengurangan",
    perkalian: "Perkalian",
    pembagian: "Pembagian",
  },
  searchParams: {
    materi: "materi",
    name: "name",
    level: "level",
  }
};
