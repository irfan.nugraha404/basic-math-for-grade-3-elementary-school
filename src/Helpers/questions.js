import constants from "./constants";

const answers = ["A", "B", "C", "D", "E"];

const Penjumlahan = {
  1: [
    {
      id: 1,
      question:
        "Sebuah bak Pak Ali berisi ikan cupang sebanyak 300 ikan. Setelah itu Pak Ali menambahkan 356 ikan cupang kedalam bak tersebut. Maka berapakah banyak ikan cupang yang ada di bak tersebut saat ini ?",
      answers: [
        "626 ikan cupang",
        "636 ikan cupang",
        "646 ikan cupang",
        "656 ikan cupang",
      ],
      correct: "D",
    },
    {
      id: 2,
      question:
        "Perpustakaan Cahaya Ilmu mempunyai koleksi banyak buku. Ada 305 buku fiksi dan 278 buku non fiksi. Berapa jumlah buku di perpustakaan Cahaya Ilmu",
      answers: ["583", "573", "563", "293"],
      correct: "A",
    },
    {
      id: 3,
      question:
        "Bu Fina adalah seorang pedagang baju. Di tokonya terdapat 141 buah baju laki-laki dan 295 buah baju perempuan. Jadi jumlah keseluruhan baju di toko bu Fina adalah ?",
      answers: ["426", "446", "416", "436"],
      correct: "D",
    },
    {
      id: 4,
      question:
        "Pak Juan memiliki dua kebun mangga yang luas. Pada bulan ini kebun pertama menghasilkan 419 buah mangga, sedangkan kebun kedua menghasilkan 310 buah mangga. Berapa jumlah buah mangga yang dipanen Pak Juan bulan ini ?",
      answers: ["729", "739", "749", "759"],
      correct: "A",
    },
    {
      id: 5,
      question:
        "Pak Andre mempunyai dua bidang sawah berbentuk persegi panjang. Sawah pertama mempunyai keliling 317 meter sedangkan sawah yang kedua memiliki keliling 258 meter. Jadi keliling dua sawah Pak Andre adalah ?",
      answers: ["535", "645", "575", "675"],
      correct: "C",
    },
  ],
  2: [
    {
      id: 1,
      question:
        "Pada musim panen dihasilkan dua keranjang buah apel. Keranjang pertama berisi 510 buah apel. Keranjang kedua berisi 213 buah apel. Berapa jumlah buah apel dalam dua keranjang tersebut ?",
      answers: [
        "723",
        "725",
        "730",
        "740",
      ],
      correct: "A",
    },
    {
      id: 2,
      question:
        "Kebun Pak Agus ditanami pohon pepaya dan pisan. terdapat 430 pohon pisang dan 350 pohon pepaya. Jadi, jumlah pohon pisan dan pepaya di kebun Pak Agus adalah...",
      answers: ["615", "670", "780", "790"],
      correct: "C",
    },
    {
      id: 3,
      question:
        "Bu Ulfa memiliki usaha roti yang besar. Pada hari kamis ia mendapat pesanan 412 roti, sedangkan pada hari Jum'at ia mendapat pesanan 389 roti. Berapa jumlah pesanan roti yang di terima Bu Ulfa pada hari kamis dan jum'at ?",
      answers: ["780", "800", "801", "812"],
      correct: "C",
    },
    {
      id: 4,
      question:
        "Desa Sukatani memiliki jumlah warga sebanyak 406 laki-laki dan 299 perempuan. Berapa jumlah warga di desa Sukatani ?",
      answers: ["715", "705", "609", "615"],
      correct: "B",
    },
    {
      id: 5,
      question:
        "Hari ini para siswa SMP Mutiara Hati mengikuti upacara bendera. Ada 454 siswa laki-laki dan 318 siswa perempuan yang mengikuti upacara. Jadi, jumlah siswa yang mengikuti upacara bendera adalah...",
      answers: ["770", "772", "775", "780"],
      correct: "B",
    },
  ],
  3: [
    {
      id: 1,
      question:
        "Kebun anggur Pak Albab pada bulan Agustus menghasilkan 293 kg anggur. Sedangkan pada bulan September meningkat pesat hasilnya mencapai 639 kg anggur. Jumlah panen anggur di kebun Pak Albab pada bulan Agustus dan September adalah...",
      answers: [
        "890",
        "900",
        "930",
        "932",
      ],
      correct: "D",
    },
    {
      id: 2,
      question:
        "Ada dua truk yang mengangkut semangka. Truk pertama membawa muatan 435 buah semangka. Lalu truk yang kedua membawa 478 buah semangka. Jumlah muatan semangka yang dibawa kedua truk jika digabung adalah... kg.",
      answers: ["905", "911", "913", "915"],
      correct: "C",
    },
    {
      id: 3,
      question:
        "Sebuah pabrik memiliki jumlah karyawan laki-laki sebanyak 590 orang dan perempuan sebanyak 400 orang. Jadi, jumlah seluruh karyawan di pabrik tersebut adalah …. orang.",
      answers: ["990", "995", "944", "947"],
      correct: "A",
    },
    {
      id: 4,
      question:
        "Para penduduk bergotong royong melakukan reboisasi di hutan selama dua hari. Mereka berhasil menanam 576 bibit pohon jati di hari pertama dan 394 bibit pohon jati di hari kedua. Jadi, jumlah bibit pohon jati yang berhasil di tanam adalah...",
      answers: ["970", "973", "975", "980"],
      correct: "A",
    },
    {
      id: 5,
      question:
        "Pak Bagas adalah seorang pedagang semangka. Di kios tokonya sekarang terdapat 610 buah semangka. Hari ini ia menambah semangka di tokonya sebanyak 315 buah. Jadi, jumlah semangka di kios toko Pak Bagas sekarang adalah...",
      answers: ["920", "925", "933", "940"],
      correct: "B",
    },
  ],
};

const Pengurangan = {
  1: [
    {
      id: 1,
      question:
        "Pak Sandi merupakan seorang pengusaha mebel. Pak Sandi mendapat pesanan untuk membuat 380 meja. Pada bulan ini sebanyak 167 meja sudah jadi. Banyak meja yang sudah jadi ada ... meja.",
      answers: ["183", "173", "193", "213"],
      correct: "D",
    },
    {
      id: 2,
      question:
        "Ayah berhasil memanen 467kg ikan lele dari tambaknya. Sebanyak 151 kg sudah terjual. Berat ikan lele yang tersisa adalah ... kg",
      answers: ["316", "356", "306", "126"],
      correct: "A",
    },
    {
      id: 3,
      question:
        "Di balai desa Tambakromo terdapat 500 kusi. Saat ada hajatan keluarga, Pak Anwar meminjam 319 kursi dari sana. Sisa kursi di balai desa sekarang berjumlah ... kursi",
      answers: ["191", "291", "391", "181"],
      correct: "D",
    },
    {
      id: 4,
      question:
        "SMA Brilian mempunyai 402 murid. Sebanyak 214 merupakan murid laki-laki. Maka jumlah murid perempuan di SMA Brilian berjumlah ... murid",
      answers: ["186", "189", "188", "198"],
      correct: "C",
    },
    {
      id: 5,
      question:
        "Seorang pedagang mempunyai persediaan 363 kg buah apel di pasar. Setelah berjualan dua hari, buah apelnya terjual 129 kg. Maka persediaan apel pedagang tersebut sisa ... buah",
      answers: ["245", "234", "254", "224"],
      correct: "B",
    },
  ],
  2: [
    {
      id: 1,
      question:
        "Bu Tika seorang penjual kain batik. Pada awal bulan, Bu Tika mempunyai stok 538 meter kain batik. Pada akhir bulan, ternyata sudah terjual sebanyak 105 meter kain batik. Jadi, stok kain batik di toko Bu Tika sisa... meter.",
      answers: ["433", "440", "445", "450"],
      correct: "A",
    },
    {
      id: 2,
      question:
        "Bu Jeni seorang pembuat bros. Pada bulan ini ia membeli 451 mutiara bros dengan aneka warna. Sebanyak 118 mutiara bros telah digunakan. Jadi, sisa mutiara bros yang belum digunakan sebanyak …. butir.",
      answers: ["330", "333", "345", "350"],
      correct: "B",
    },
    {
      id: 3,
      question:
        "Bu Yunita adalah seorang pengusaha sandal. Seminggu ini Bu Yunita telah menjual 598 sandal. Sebanyak 213 buah yang terjual adalah sandal laki-laki, sisanya adalah sandal perempuan. Jadi, sandal perempuan yang terjual selama seminggu ini sebanyak …. buah.",
      answers: ["370", "380", "385", "390"],
      correct: "C",
    },
    {
      id: 4,
      question:
        "Bu Lastri baru saja panen stroberi di kebunnya. Hasil panennya kali ini seberat 695 kg. Kemudian ia menjual 204 kg buah kepada pedagang pasar. Sisanya akan dijual sendiri di toko miliknya. Jadi, buah stroberi dijual di toko seberat …. kg.",
      answers: ["390", "391", "490", "491"],
      correct: "D",
    },
    {
      id: 5,
      question:
        "Bu Lisa seorang penjual masker kain. Bu Lisa pada bulan ini telah memproduksi 546 masker dengan beragam variasi. Jika pada bulan ini telah terjual 181 masker. Maka sisa masker yang belum terjual adalah …. buah.",
      answers: ["365", "370", "375", "380"],
      correct: "A",
    },
  ],
  3: [
    {
      id: 1,
      question:
        "Pak Anto mempunyai peternakan kambing yang besar. Ia memiliki 709 ekor kambing, sebanyak 224 ekor merupakan kambing betina. Jadi, jumlah kambing jantan di peternakan Pak Anto berjumlah …. ekor.",
      answers: ["485", "490", "515", "520"],
      correct: "A",
    },
    {
      id: 2,
      question:
        "Seorang pedagang menurunkan semangka dari sebuah truk. Ternyata ada 757 buah semangka yang ada dalam truk tersebut. Pedagang tersebut mengelompokan semangkannya menjadi dua kelompok, yaitu semangka besar dan kecil. Semangka besar terkumpul 328 buah. Jadi, buah semangka yang berukuran kecil berjumlah ….",
      answers: ["425", "427", "429", "433"],
      correct: "C",
    },
    {
      id: 3,
      question:
        "3.	Bu Sari sedang mendata jumlah baju di toko miliknya. Terdata ada sebanyak 638 baju di tokonya. Terdiri dari baju anak-anak dan baju dewasa. Jika sebanyak 149 baju adalah baju anak-anak, maka jumlah baju dewasa di toko Bu Sari sekarang adalah …. Baju.",
      answers: ["480", "489", "495", "500"],
      correct: "B",
    },
    {
      id: 4,
      question:
        "Pak Naufal mempunyai 806 kg cabai dari hasil panen sawahnya. Kemudian ada beberapa tetangga yang membeli cabai miliknya. Kini cabai Pak Naufal tersisa 217 kg. Jadi, jumlah cabai yang dibeli tetangganya adalah seberat …. kg.",
      answers: ["490", "495", "580", "589"],
      correct: "D",
    },
    {
      id: 5,
      question:
        "Bu Rika mempunyai usaha pembuatan seragam. Pada bulan ini ia mendapat pesanan untuk membuat 732 seragam olahraga. Sebanyak 237 seragam sudah jadi, maka jumlah pesanan yang belum jadi adalah …. seragam.",
      answers: ["485", "487", "490", "495"],
      correct: "D",
    },
  ],
};

const Perkalian = {
  1: [
    {
      id: 1,
      question:
        "Nino mempunyai 11 kaleng berisi kelereng. Setiap kaleng berisi 19 butir kelereng. Jadi banyak kelereng milik Nino adalah ... butir",
      answers: ["209", "208", "207", "219"],
      correct: "A",
    },
    {
      id: 2,
      question:
        "Pak Amar seorang penjual buah. Di tokonya terdapat 14 keranjang buah jeruk. Setiap keranjangnya itu berisi 18 buah jeruk. Jadi, banyak buah jeruk yang ada di toko Pak Amar adalah ... buah",
      answers: ["242", "262", "272", "252"],
      correct: "D",
    },
    {
      id: 3,
      question:
        "Terdapat 13 gulungan pita di toko Cahaya Brilian. Jika setiap gulungan pita mempunyai panjang 16 meter. Jadi, panjang seluruh pita di toko Cahaya Brilian adalah ... meter",
      answers: ["209", "208", "228", "218"],
      correct: "B",
    },
    {
      id: 4,
      question:
        "Bu Mega adalah penjual telur di pasar. Hari ini ada 18 orang yang membeli telurnya. Setiap orang membeli 10 kg telur. Jadi, jumlah telur yang di jual bu Mega hari ini adalah ... kg",
      answers: ["118", "800", "108", "180"],
      correct: "D",
    },
    {
      id: 5,
      question:
        "Ibu baru saja memanen terong dari kebunnya. Ia berhasil mengumpulkan 11 keranjang terong. Setiap keranjang berisi 15 terong. Jadi, banyak terong yang dipanen ibu adalah...",
      answers: ["176", "165", "167", "175"],
      correct: "B",
    },
  ],
  2: [
    {
      id: 1,
      question:
        "Pak Bobi mempunyai 13 kandang ayam. Setiap kandang berisi 16 anak ayam. Jadi, jumlah keseluruhan anak ayam milik Pak Bobi adalah...",
      answers: ["176", "186", "207", "208"],
      correct: "D",
    },
    {
      id: 2,
      question:
        "Toko Bu Ratih memiliki persediaan 18 kotak pensil. Jika setiap kotak berisi 12 pensil. Maka, banyak pensil yang ada di toko Bu Ratih adalah...",
      answers: ["205", "210", "216", "218"],
      correct: "C",
    },
    {
      id: 3,
      question:
        "Sari membawa 12 plastik buah salak. Setiap plastik berisi 17 buah salak. Jadi, banyak buah salak yang dibeli Sari adalah... buah.",
      answers: ["209", "204", "214", "194"],
      correct: "B",
    },
    {
      id: 4,
      question:
        "Pak Irwan memiliki 19 karung buah alpukat. Setiap karung berisi 15 kg buah alpukat. Jadi, berat buah alpukat seluruhnya adalah...kg.",
      answers: ["285", "290", "292", "295"],
      correct: "A",
    },
    {
      id: 5,
      question:
        "Pak Heru mampu membuat 19 kandang burung setiap bulan. Jika ia telah bekerja sebagai pembuat kandang burung selama 12 bulan. Maka, jumlah kandang burung yang telah Pak Heru buat adalah... kandang.",
      answers: ["257", "228", "277", "237"],
      correct: "B",
    },
  ],
  3: [
    {
      id: 1,
      question:
        "Pak Isa memiliki 20 karung buah sirsak. Setiap karung berisi 15 kg buah sirsak. Jadi, berat buah sirsak seluruhnya adalah... kg.",
      answers: ["300", "310", "320", "330"],
      correct: "A",
    },
    {
      id: 2,
      question:
        "Pak Juki memiliki 20 pohon pepaya. Setiap panen rata-rata setiap pohon menghasilkan 17 kg buah pepaya. Jadi, jumlah pepaya yang dihasilkan Pak Juki setiap panen adalah... kg.",
      answers: ["320", "340", "360", "370"],
      correct: "B",
    },
    {
      id: 3,
      question:
        "Sebuah lapangan parkir terdapat 22 baris sepeda motor. Jika setiap baris terdapat 20 sepeda motor. Maka, banyaknya sepeda motor di lapangan parkir tersebut adalah...",
      answers: ["400", "420", "440", "450"],
      correct: "C",
    },
    {
      id: 4,
      question:
        "Wildan membeli 25 batang bambu. Setiap batang panjangnya 15 meter. Jadi, panjang seluruh bambu yang dibeli Wildan adalah... meter.",
      answers: ["375", "385", "395", "400"],
      correct: "A",
    },
    {
      id: 5,
      question:
        "Pak Dadang mempunyai peternakan sapi yang besar. Disana terdapat 25 kandang berisi sapi. Jika setiap kandang terdapat 14 sapi. Maka, banyak sapi di peternakan Pak Dadang adalah...",
      answers: ["250", "300", "320", "350"],
      correct: "D",
    },
  ],
};

const Pembagian = {
  1: [
    {
      id: 1,
      question:
        "Tika membeli 100 buah apel. Dia memasukkan buah apel tersebut ke dalam 10 kantong plastik dengan jumlah sama. Maka setiap kantong plastik berisi buah apel sebanyak ... buah",
      answers: ["10", "11", "12", "13"],
      correct: "A",
    },
    {
      id: 2,
      question:
        "Bu Lestari baru saja memanen 144 buah pepaya dari kebunnya. Dia ingin menaruh pepaya tersebut kedalam 8 keranjang dengan jumlah sama banyak. Maka pepaya yang terdapat pada setiap keranjang akan berjumlah ... buah",
      answers: ["16", "17", "18", "19"],
      correct: "C",
    },
    {
      id: 3,
      question:
        "Arif membeli 117 butir kelereng. Kelereng tersebut dimakukkan kedalam 9 kotak secara sama rata. Maka masing-masing kotak itu berisi ... butir kelereng",
      answers: ["11", "14", "12", "13"],
      correct: "D",
    },
    {
      id: 4,
      question:
        "Rara mempunyai pita sepanjang 135 cm. Dia memotong pita tersebut menjadi 9 bagian sama panjang. Maka panjang setiap potongan pita tersebut adalah ... cm",
      answers: ["15", "19", "16", "18"],
      correct: "A",
    },
    {
      id: 5,
      question:
        "Ibu mempunyai 180 buah piring keramik. Piring-piring tersebut akan disimpan dalam kardus. Jika setiap kardus berisi 12 buah piring, maka banyak kardus yang dibutuhkan ibu adalah ...",
      answers: ["16", "17", "15", "14"],
      correct: "C",
    },
  ],
  2: [
    {
      id: 1,
      question:
        "Rizal membawa 182 buah permen. Dia ingin membagikan permen itu kepada teman-temannya secara sama rata. Jika setiap orang menerima 13 buah permen, maka teman-teman Rizal yang mendapat permen berjumlah...",
      answers: ["18", "14", "17", "13"],
      correct: "B",
    },
    {
      id: 2,
      question:
        "Sebuah ember hanya mampu menampung 11 liter air. Maka jumlah ember yang dibutuhkan untuk bisa menampung 231 liter air adalah.. ember.",
      answers: ["19", "22", "23", "21"],
      correct: "D",
    },
    {
      id: 3,
      question:
        "Sebuah jalan di taman mempunyai panjang 240 meter. Jika setiap 16 meter akan dipasangi lampu taman, maka jumlah lampu taman yang dibutuhkan di jalan tersebut adalah... lampu.",
      answers: ["18", "15", "16", "14"],
      correct: "B",
    },
    {
      id: 4,
      question:
        "Febi akan mendapat pesanan 209 buah gantungan kunci. Jika setiap hari ia bisa membuat 11 gantungan kunci. Maka ia bisa menyelesaikan semua pesanan tersebut dalam waktu... hari.",
      answers: ["19", "18", "17", "16"],
      correct: "A",
    },
    {
      id: 5,
      question:
        "Naufal baru saja mencetak 198 lembar kartu nama. Kartu nama tersebut dimasukkan ke dalam 9 kotak secara sama rata. Jadi, masing-masing kotak berisi... kartu nama.",
      answers: ["21", "23", "24", "22"],
      correct: "D",
    },
  ],
  3: [
    {
      id: 1,
      question:
        "Sebuah konveksi baju mendapatkan pesanan untuk membuat 208 seragam sekolah. Pesanan seragam tersebut dijahit oleh 13 orang. Jika setiap orang menjahit seragam dengan jumlah sama, maka jumlah seragam yang dijahit masing-masing karyawan adalah... seragam.",
      answers: ["16", "18", "17", "19"],
      correct: "A",
    },
    {
      id: 2,
      question:
        "Hari ini Bu Fitri membuat 228 bungkus keripik pisang untuk dijual ke pasar. Jika ia menaruh keripik pisang tersebut ke dalam 12 kardus dengan jumlah yang sama banyak. Maka setiap kardus berisi... bungkus keripik.",
      answers: ["17", "16", "18", "19"],
      correct: "D",
    },
    {
      id: 3,
      question:
        "Nindi memiliki koleksi 225 perangko. Dia ingin menempelkan perangkonya ke dalam sebuah buku. Jika setiap halaman buku hanya mampu ditempeli 15 perangko, maka jumlah halaman yang dibutuhkan untuk menempelkan semua perangko adalah... halaman.",
      answers: ["18", "17", "16", "15"],
      correct: "D",
    },
    {
      id: 4,
      question:
        "Popi adalah seorang pedagang buah. Di tokonya terdapat 220 kg buah anggur yang ditaruh didalam 11 keranjang buah. Jika setiap keranjang mempunyai berat anggur yang sama, maka setiap keranjang tersebut berisi... kg buah anggur.",
      answers: ["17", "18", "19", "20"],
      correct: "D",
    },
    {
      id: 5,
      question:
        "Bu Tutik mendapat pesanan untuk membuat 196 seragam olahraga. Jika setiap hari ia bisa membuat 7 buah seragam olahraga. Maka Bu Tutik bisa menyelesaikan pesanan tersebut dengan waktu... hari.",
      answers: ["28", "27", "26", "25"],
      correct: "A",
    },
  ],
};

export default {
  answers,
  [constants.materi.penjumlahan]: Penjumlahan,
  [constants.materi.pengurangan]: Pengurangan,
  [constants.materi.perkalian]: Perkalian,
  [constants.materi.pembagian]: Pembagian,
};
