import constants from "./constants";

export default {
    bg1: "/assets/bg/1.png",
    bg2: "/assets/bg/2.png",
    user: "/assets/icons/profile.jpg",
    video: {
        [constants.materi.penjumlahan]: "https://www.youtube.com/embed/bYsIAY_FvRA",
        [constants.materi.pengurangan]: "https://www.youtube.com/embed/6_dwjWESOuc",
        [constants.materi.perkalian]: "https://www.youtube.com/embed/FB7Ect2RBKQ",
        [constants.materi.pembagian]: "https://www.youtube.com/embed/ZMQ_XgmTcRE",
    }
}